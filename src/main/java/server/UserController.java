package server;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import model.UserRegisterModel;
import repo.DataBaseConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Random;

public class UserController implements HttpHandler {
    private Gson gson = new Gson();

    private static final String HEADER_ALLOW = "Allow";
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final Charset CHARSET = StandardCharsets.UTF_8;
    private static final int STATUS_OK = 200;
    private static final int STATUS_METHOD_NOT_ALLOWED = 405;
    private static final int NO_RESPONSE_LENGTH = -1;
    private static final String METHOD_POST = "POST";
    private static final String ALLOWED_METHODS = METHOD_POST;


    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        switch (httpExchange.getRequestURI().getPath()){
            case "/hello":
                httpExchange.sendResponseHeaders(200,"httpmultiplerequesthandler".length());
                OutputStream os = httpExchange.getResponseBody();
                os.write("httpmultiplerequesthandler".getBytes());
                os.close();
                break;
            case "/api/register":
                final Headers headers = httpExchange.getResponseHeaders();
                final String requestMethod = httpExchange.getRequestMethod().toUpperCase();
                if (requestMethod.equals(METHOD_POST)){
                    System.out.println("post");
                    final String responseBody = "{\"result\":\"verification code sent\"}";
                    headers.set(HEADER_CONTENT_TYPE,String.format("application/json; charset=%s",CHARSET));
                    final byte[] rawResponseBody = responseBody.getBytes(CHARSET);

                    InputStreamReader isr = new InputStreamReader(httpExchange.getRequestBody(),"utf8");
                    BufferedReader br = new BufferedReader(isr);
                    int b;
                    StringBuilder buf = new StringBuilder();
                    while ((b=br.read())!=-1){
                        buf.append((char) b);
                    }
                    UserRegisterModel model = gson.fromJson(buf.toString(),UserRegisterModel.class);
                    Random random = new Random();
                    DataBaseConfig.sendVerificationCode(model,random.nextInt(9)*1000+random.nextInt(9)*100+random.nextInt(9)*10+random.nextInt(9));

                    httpExchange.sendResponseHeaders(STATUS_OK,rawResponseBody.length);
                    httpExchange.getResponseBody().write(rawResponseBody);

                } else {
                    System.out.println("not allowed");
                    headers.set(HEADER_ALLOW,ALLOWED_METHODS);
                    httpExchange.sendResponseHeaders(STATUS_METHOD_NOT_ALLOWED,NO_RESPONSE_LENGTH);
                }


                break;
            case "/api/confirm":

                break;
            default:
                String defaulResponse = "Page Not Found";
                httpExchange.sendResponseHeaders(404,defaulResponse.length());
                System.out.println("undandled request"+httpExchange.getRequestURI().getPath());
                OutputStream defaultOS = httpExchange.getResponseBody();
                defaultOS.write(defaulResponse.getBytes());
                defaultOS.close();
                break;
        }


    }
}
