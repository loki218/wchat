package server;

import com.sun.net.httpserver.HttpServer;
import io.sentry.Sentry;
import repo.DataBaseConfig;
import util.SentryUtil;

import javax.websocket.DeploymentException;
import java.net.InetSocketAddress;
import java.util.Scanner;

public class Server {

    public static void main(String[] args){
        org.glassfish.tyrus.server.Server server = new org.glassfish.tyrus.server.Server("localhost",8025,"/ws",null,ServerEndpoint.class);

        try {
//            Sentry.init();

            HttpServer httpServer = HttpServer.create(new InetSocketAddress(8080),0);
            httpServer.createContext("/api",new UserController());
            httpServer.setExecutor(null);
            httpServer.start();

            server.start();
            DataBaseConfig db = new DataBaseConfig();
            db.initDB();

            System.out.println("Press any key to stop the server..");
            new Scanner(System.in).nextLine();
//            throw new Exception("you shouldn't call this!");
        } catch (DeploymentException e){
            SentryUtil.logWithStaticAPI(e);
//            Sentry.capture(e);
        }
        catch (Exception e){
            SentryUtil.logWithStaticAPI(e);
//            Sentry.capture(e);
        }
        finally {
//            server.stop();
        }
    }
}
