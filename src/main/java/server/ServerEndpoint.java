package server;

import com.google.gson.Gson;
import model.Message;
import model.MessageDecoder;
import model.MessageEncoder;

import javax.websocket.*;
import java.awt.*;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@javax.websocket.server.ServerEndpoint(value = "/chat",decoders = MessageDecoder.class)
public class ServerEndpoint {

    static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

    @OnOpen
    public void onOpen(Session session){
        System.out.println(java.lang.String.format("%s joined the chat room.",session.getId()));
        peers.add(session);
//        Message message = new Message();
//        message.setSender("Server");
//        message.setContent("watafak");
//        message.setReceived(new Date());
//        try {
//            session.getBasicRemote().sendObject(message);
//        }catch (EncodeException e){
//            e.printStackTrace();
//        } catch (IOException e){
//            e.printStackTrace();
//        }
    }

    @OnMessage
    public void onMessage(Message message,Session session)throws IOException, EncodeException{
        String user = message.getSender();
        if (user == null){
            session.getUserProperties().put("user",message.getSender());
        }
        if ("quit".equalsIgnoreCase(message.getContent())){
            session.close();
        }

        System.out.println(java.lang.String.format("[%s:%s] %s",message.getSender(),message.getReceived(),message.getContent()));

        for(Session peer:peers){
            if (!session.getId().equals(peer.getId())){
                Gson gson = new Gson();
                peer.getBasicRemote().sendText(gson.toJson(message));
            }
        }
    }

    @OnClose
    public void onClose(Session session,CloseReason reason) throws IOException,EncodeException{
        System.out.println(java.lang.String.format("%s left the chat room. Reason:%s",session.getId(),reason.getReasonPhrase()));
        peers.remove(session);

        for(Session peer:peers){
            Message chatMessage = new Message();
            chatMessage.setSender("Server");
            chatMessage.setContent(java.lang.String.format("%s left the chat room.",(String)session.getUserProperties().get("user")));
            chatMessage.setReceived(new Date());
            Gson gson = new Gson();
            peer.getBasicRemote().sendText(gson.toJson(chatMessage));
        }
    }
}
