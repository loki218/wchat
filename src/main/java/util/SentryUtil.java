package util;

import io.sentry.Sentry;
import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;
import io.sentry.event.Breadcrumb;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;

public class SentryUtil {

    public static void logWithStaticAPI(Exception e){
        Sentry.init();
        Sentry.capture(e);
    }
}
