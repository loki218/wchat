package repo;

import model.Message;
import model.UserRegisterModel;
import util.SentryUtil;

import java.sql.*;
import java.util.Iterator;
import java.util.LinkedList;

public class DataBaseConfig {
    public DataBaseConfig(){
        Connection conn = null;
        LinkedList listOfMessages = new LinkedList();
        conn = connectToDb();
        populateListOfMsg(conn,listOfMessages);
        printMsgs(listOfMessages);


    }

    private void populateListOfMsg(Connection conn,LinkedList listOfMsg){
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id,message FROM test order by id");
            while (rs.next()){
                Message message = new Message();
                message.setContent(rs.getString("message"));
                listOfMsg.add(message);
            }
            rs.close();
            st.close();
        }catch (SQLException se){
//            se.printStackTrace();
            SentryUtil.logWithStaticAPI(se);
        }
    }
    public void printMsgs(LinkedList listOfMsg){
        Iterator it = listOfMsg.iterator();
        while (it.hasNext()){
            System.out.println(((Message)it.next()).getContent());
        }
    }
    public static void initDB(){
        try {
            Connection connection = connectToDb();
            Statement statement = connection.createStatement();
            statement.executeUpdate("create table if not exists messages(message_id integer ,sender_id varchar(50), message_text text,message_file text, message_file_type text, message_date varchar(50),message_status varchar(50) )");
            statement.executeUpdate("create table if not exists users(user_id integer,user_nick_name varchar(50), user_phone varchar(50), user_full_name text,user_logo text,user_status varchar(50),user_token text)");
            statement.executeUpdate("create table if not exists phone_verification(user_phone varchar(50), verification_code integer)");
            statement.close();

        }catch (SQLException e){
            SentryUtil.logWithStaticAPI(e);
            e.printStackTrace();
        }
    }
    public static void addUser(UserRegisterModel model){
        try {
            Connection connection = connectToDb();
            Statement statement = connection.createStatement();
            statement.executeUpdate("insert into users values(1,'','"+model.getUser_phone()+"','','','','')");
            statement.close();
        }catch (SQLException e){
            SentryUtil.logWithStaticAPI(e);
            e.printStackTrace();
        }
    }
    public static void sendVerificationCode(UserRegisterModel model,int code){
        try {
            System.out.println(code);
            Connection connection = connectToDb();
            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ResultSet resultSet = statement.executeQuery("select * from phone_verification where user_phone='"+model.getUser_phone()+"'");
            int rowcount = resultSet.last()?resultSet.getRow():0;
            if (rowcount==0){
                statement.executeUpdate("insert into phone_verification values ('"+model.getUser_phone()+"',"+code+")");
            } else {
                statement.executeUpdate("update phone_verification set verification_code = "+code+" where user_phone='"+model.getUser_phone()+"'");
            }
            statement.close();
        } catch (SQLException e){
            SentryUtil.logWithStaticAPI(e);
            e.printStackTrace();
        }

    }
    public static Boolean verifyCode(UserRegisterModel model,int code){
        try {
            Connection connection = connectToDb();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from phone_verification where user_phone='"+model.getUser_phone()+"'");
            while (resultSet.next()){
                if (code == resultSet.getInt("verification_code")){
                    addUser(model);
                    return true;
                }
            }
            return false;

        }catch (SQLException e){
            e.printStackTrace();
            SentryUtil.logWithStaticAPI(e);
            return false;
        }
    }

    private static Connection connectToDb(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://localhost/ewp","loki","12345");

        }catch (SQLException e){
//            e.printStackTrace();
//            System.exit(2);
            SentryUtil.logWithStaticAPI(e);
        }

        return conn;
    }
}
